// 2D
const Square = require("./square");
const Rectangle = require("./rectangle");
const Triangle = require("./triangle");

// 3D
const Cube = require("./cube");
const Beam = require("./beam");
const Tube = require("./tube");
const Cone = require("./cone");

module.exports = { Square, Rectangle, Triangle, Cube, Beam, Tube, Cone };

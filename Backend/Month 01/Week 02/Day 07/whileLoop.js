// while loop
let i = 0;

while(i <= 5) {
    console.log(i);
    i++;
}

i = 0;
// Another case
while(i <= 5) {
    i++;
    console.log(i);    
}
// Declare Array
let fruits = ["Apple", "Orange", "Pineaple", "Mango", "Melon"];
console.log(fruits.length);

// use for
for (let i = 0; i < fruits.length; i++) {
    console.log(fruits[i]);
}
// lenght : ngasih tau jumlah data ada berapa di dalam baris


// use while
let i = 0;

while (i < fruits.length) {
    console.log(fruits[i]);
    i++;
}


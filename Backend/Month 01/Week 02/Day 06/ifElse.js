// boolean only have two value, true or false
const isTodayRaining = true;

if (isTodayRaining) {
    // if today raining
    console.log("Use umbrella!");
}
else {
    // else
    console.log("Just bring umbrella not used!");
}
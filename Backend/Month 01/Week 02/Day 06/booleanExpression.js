/*boolean expression*/
//equal operator
let a = 1 == 2; // false
let b = 1 != 2 // true

console.log(a);
console.log(b);

// strict equal (tipe data harus sama)
let c = "1" === 1; // false 
console.log(c);

// less strict equal (tipe data nggak harus sama)
let d = "1" == 1; // true
console.log(d);
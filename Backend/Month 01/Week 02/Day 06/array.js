// array data type
let fruits = ["Apple", "Orange", "Pineaple", "Watermelon"];

// array di hitung dari angka 0
console.log("Fruits: " + fruits); // it will call all fruits
console.log("Fruits(0) " + fruits[0]); // it will call fruits index 0
console.log("Fruits(3) " + fruits[3]); // it will call fruits index 3

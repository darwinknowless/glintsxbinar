// Switch case
// Mirip if else

let option = 4;

switch (option) {

  case 1:
  case 4:  
    console.log("This is One");
    break;

  case 2:
    console.log("This is two");
    break;

  case 3:
    console.log("This is three");
    break;

  default:
    console.log("This is default");
}

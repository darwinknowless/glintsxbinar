let a = "Hello";

let literal = `${a} World`;
console.log(literal); // Hello World

// Or even an expresison

let random = `2 + 2 = ${2 + 2}`;
console.log(random) // 2 + 2 = 4

//Hello function
function hello() {
    console.log("Hello!");
    console.log("World")
}
//World function
function world() {
    console.log("World")
    console.log("Hello!"); 
}

hello(); // call hello function
world(); // call world function
hello(); // call hello function
/* using let */
let a = 10;
console.log(a); // Declare with value

/* Or */
let b; //Declare
b = 20; // Assign value to b
console.log(b); // Declare with Assign b

/* using let */
let c = 30;
console.log(c); // Declare with value

/* Or */
let d; //Declare
d = 40; // Assign value to b
console.log(d); // Declare with Assing d

/* Using Conts */
let z = 50;
console.log(z); // Declare with Declare cost

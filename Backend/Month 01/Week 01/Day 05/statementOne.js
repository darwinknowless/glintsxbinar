// This is Single Statement
console.log("Hello World"); // Print Hello World!

/* Two Statement */
console.log("First Statement");
console.log("Second Statement");
// Or
console.log("First Statement")
console.log("Second Statement")
/* Using let */
let a; // Create/Declare variable a
a = 10; // Assign a with 10
console.log(a);
a = 21; // Reassign a with 21
console.log(a);
// leet a = 24 // it will error because it has been declare

var b;
b = 7;
console.log(b);
b = 22;
console.log(b);
var b = 30;
console.log(b);